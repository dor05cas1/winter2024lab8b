import java.util.Scanner;

public class BoardGameApp{
	public static void main (String[] args){
		Scanner scanner = new Scanner(System.in);
		
		Board boardgaem = new Board();
		//Greet the beautiful player
		System.out.println("Wecome to castke gaem");
		
		int numCastles = 7;
		int turns = 0;
		
		//Playing the game: with a total of 8 turns
		while(numCastles > 0 && turns < 8){
			
			System.out.println(boardgaem);
			System.out.println("Number of castles left to find: " + numCastles);
			System.out.println("Current number of turns: " + turns);
			
			//Ask the player to politely enter 2 values: one for number that represent a row, the other, a column...
			System.out.println("Enter 2 integers that represent a row and a column of the board...");
			int row = scanner.nextInt();
			int col = scanner.nextInt();
		
		
			int token = boardgaem.placeToken(row, col);
			
			//Checks for valid row/column inputs made by wonderful user
			while(token < 0){
				System.out.println("Please re-enter 2 valid numbers that represent a row and a column in the board");
				
				row = scanner.nextInt();
				col = scanner.nextInt();
				token = boardgaem.placeToken(row, col);
			}
			//Turns go up by 1 when you hit a wall.
			if(token == 1){
				System.out.println("You hit a wall...");
				System.out.println("Turns go up by 1");
				turns++;
			}
			//Turns go up by 1 when you find a castle & number of castles goes down by 1.
			else if(token == 0){
				System.out.println("Castle successfully placed!");
				System.out.println("Turns go up by 1." + "\n" + "Number of castles reduced by 1");
				turns++;
				numCastles--;
			}
		}
		System.out.println(boardgaem);
		
		//If no more castles left within 8 turns, they win game.
		if(numCastles == 0){
			System.out.println("You won the gaem congrats king!");
		}
		//If they did not find all castles within 8 turns... they lose.
		else{
			System.out.println("You lost the gaem!");
		}
				

	}
}