import java.util.Random;
public class Board{
	
	//Add fields
	private Tile[][] grid;
	private final int SIZE;
	
	//Add a constructor
	public Board(){
		this.SIZE = 5;
		this.grid = new Tile[this.SIZE][this.SIZE];
		
		Random rng = new Random();
		
		for(int i = 0 ; i < this.grid.length; i++){
			int index = rng.nextInt(this.grid[i].length);
			for(int j = 0; j < this.grid[i].length; j++){
				grid[i][j] = Tile.BLANK;
				grid[i][index] = Tile.HIDDEN_WALL;
			}
		}
	}
	
	//Overide the toString() method
	public String toString(){
		String tileBoard = "";
		
		for (int i = 0; i < this.grid.length; i ++){
			for(int j = 0; j < this.grid[i].length; j++){
				
				tileBoard += this.grid[i][j].getName() + " ";
			}
			tileBoard += "\n";
		}
		return tileBoard;
	}
	
	//Instance method: placeToken
	public int placeToken(int row, int col){
		//Checks for invalid values of row and columns...
		if(row >= this.SIZE || col >= this.SIZE || row < 0 || col < 0){
			return -2;
		}
		//Checks if there is already a castle or wall at the given row and column
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL){
			return -1;
		}
		//Sets position to WALL value if there is a hidden wall...
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		//If theres no hidden wall, it becomes a castle.
		else{
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
		
	}
	
	
	

		
		
		
		
		
		
	





}